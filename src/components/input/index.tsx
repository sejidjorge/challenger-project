import { useEffect, useState } from 'react';
import { InputProps } from '../../types/inputTypes';
import { Container, InputContainer, InputLabel, InputStyled } from './styles';

const Input = ({ label, value, setValue }: InputProps) => {
  const [teste, setTeste] = useState<string>()
  const validate = /[^0-9.]+/g;  

useEffect(() => {
  if(!isNaN(Number(teste))) {
    setValue(Number(teste))
  } else {
    setValue(null)
  }
}, [teste])


  return (
    <>
      <Container>
        <InputContainer>
          <InputStyled
            id={label}
            name={label}
            required
            onChange={(e) => setTeste(e.target.value.replace(validate, ''))}
            value={teste}
          />
          <InputLabel isEmpty={!value && value !== 0} htmlFor={label}>
            {label}
          </InputLabel>
        </InputContainer>
      </Container>
    </>
  );
};

export default Input;
