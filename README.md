
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Demo Version
Open [Demo](https://challenger-project.vercel.app/) with your browser to see demo in Vercel.

